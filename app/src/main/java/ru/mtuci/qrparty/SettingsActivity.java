package ru.mtuci.qrparty;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;


public class SettingsActivity extends AppCompatActivity {

    EditText editTextServerURL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_settings);

        editTextServerURL = (EditText)findViewById(R.id.editTextServerURL);

        // получим сохраненное значение URL
        SharedPreferences sharedPref =  this.getSharedPreferences(getString(R.string.prefs_uniqie),Context.MODE_PRIVATE);
        String str = sharedPref.getString(getString(R.string.settings_URL_key), getString(R.string.settings_default_URL));
        editTextServerURL.setText( str );

        final Button buttonSave = findViewById(R.id.buttonSave);
        final Button buttonDefault = findViewById(R.id.buttonDefault);

        buttonSave.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                saveNewURL();
            }
        });

        buttonDefault.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                editTextServerURL.setText(R.string.settings_default_URL);
                saveNewURL();
            }
        });
    }

    private void saveNewURL() {
        // сохраняем настройки
        SharedPreferences sharedPref =  this.getSharedPreferences(getString(R.string.prefs_uniqie),Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(getString(R.string.settings_URL_key), editTextServerURL.getText().toString());
        editor.apply();
    }
}
