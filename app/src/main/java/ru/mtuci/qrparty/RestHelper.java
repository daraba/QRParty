package ru.mtuci.qrparty;

import android.os.AsyncTask;
import android.util.JsonReader;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import javax.net.ssl.HttpsURLConnection;

public class RestHelper {

    // для того чтобы прослушивать события генерируемые этим классом
    public interface RestListener {
        public void onJsonReady(HashMap<String, String> map);
        public void failedRequest();
        public void failedInfo();
    }

    // указывает нам, ожидаем ли мы в данный момент ответа или нет; Если ожидаем, то будем просто рвать соединение и перезапрашивать инфу
    boolean inProgress;
    String server_url;
    String qr;

    private RestListener listener;

    public RestHelper() {
        inProgress = false;
        listener = null;
    }

    public void setListener(RestListener lst) {
        listener = lst;
    }

    /*
    The Android platform does not allow you to run network operations on the main thread of the application.
    Therefore, all your networking code must belong to a background thread.
    The easiest way to create such a thread is to use the execute() method of the AsyncTask class.
    As its only argument, execute() expects a Runnable object.
     */
    public void getInfo(String url, String qr_value){
        if( inProgress )
            return;

        qr = qr_value;
        server_url = url;
        inProgress = true;

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                // Create URL
                URL ticketEndpoint = null;
                try {
                    ticketEndpoint = new URL(server_url+qr);
                    Log.w("e",server_url+qr);
                } catch (MalformedURLException e) {
                    if( listener!=null)
                        listener.failedRequest();

                    inProgress = false;
                    e.printStackTrace();
                    return;
                }

                // Create connection
                HttpsURLConnection myConnection;
                try {
                    myConnection = (HttpsURLConnection) ticketEndpoint.openConnection();
                    if (myConnection.getResponseCode() == 200) {
                        InputStream responseBody = myConnection.getInputStream();
                        InputStreamReader responseBodyReader = new InputStreamReader(responseBody, "UTF-8");
                        JsonReader jsonReader = new JsonReader(responseBodyReader);

                        HashMap<String, String> map = new HashMap<String, String>();

                        jsonReader.beginObject(); // Start processing the JSON object
                        while (jsonReader.hasNext()) { // Loop through all keys
                            String key = jsonReader.nextName(); // Fetch the next key
                            String value = jsonReader.nextString();
                            map.put(key,value);

                        }

                        if( listener!=null) {
                            if (map.containsKey("name_1"))
                                listener.onJsonReady(map);
                            else
                                listener.failedInfo();
                        }

                        jsonReader.close();
                        myConnection.disconnect();
                        inProgress = false;
                    } else {
                        if( listener!=null)
                            listener.failedInfo();
                        inProgress = false;
                    }
                } catch (IOException e) {
                    if( listener!=null)
                        listener.failedRequest();
                    inProgress = false;
                    e.printStackTrace();
                    return;
                }

            }
        });
    }
}
